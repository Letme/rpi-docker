# Building Docker containers on RPI
ARMV = `uname -m`

ifeq ($(ARMV), "armv6l")
	ARM = "armv6l"
else
	ARM = "armhf"
endif

# What all will we use
.PHONY: all
# Targets for images
.PHONY: slovenianinlinecup
.PHONY: virtinlinerank
.PHONY: pythonpi
.PHONY: django
.PHONY: rklj
.PHONY: gcc
.PHONY: clang

all: pythonpi django rklj slovenianinlinecup virtinlinerank
	@echo "Everything built and tagged"

pythonpi:
	@echo "Building Pythonpi image"
	@docker build -t python-pi -f Dockerfile-python .
django: pythonpi
	@echo "Building Django image"
	@docker build -t django -f Dockerfile-django .
slovenianinlinecup: django
	@echo "Building slovenianinlinecup image"
	@docker build -t django-slovenianinlinecup -f Dockerfile-slovenianinlinecup .
virtinlinerank: django
	@echo "Building virrtinlinerank image"
	@docker build -t django-virtinlinerank -f Dockerfile-virtinlinerank .
rklj: django
	@echo "Building rklj image"
	@docker build -t django-rklj -f Dockerfile-rklj .

gcc:
	@echo "Building gcc image"
	@docker build -t letme/gcc-pi:1.3.0 -f Dockerfile-gcc .
	docker tag letme/gcc-pi:1.3.0 letme/gcc-pi:$(ARM)
	docker push letme/gcc-pi:$(ARM)
	#docker tag letme/gcc-pi:1.3.0 letme/gcc-pi:latest
	#docker push letme/gcc-pi:1.3.0
	#docker push letme/gcc-pi:latest


clang:
	@echo "Building clang image"
	@docker build -t letme/clang-pi:1.5.0 -f Dockerfile-clang .
	docker tag letme/clang-pi:1.5.0 letme/clang-pi:$(ARM)
	docker push letme/clang-pi:$(ARM)
	#docker push letme/clang-pi:1.5.0
	#docker push letme/clang-pi:latest



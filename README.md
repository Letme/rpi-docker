# Dockerfiles for RPIs

Problem with pre-built docker containers is that they are arch specific. That
means if they are not built on target arch that they will most likely not work
as intended. Since per-project containers always assume x86 arch it would be
easier to consider them a bit less project specific. This enables also a bit
more optimization (size is a problem) on requirements and everything built.

It is important that we keep docker tags consistent, so to build it all 
run a makefile instead of usual docker build command.
